//
//  String.swift
//  Groomefy WorkSpace
//
//  Created by Vaibhav Singh on 07/02/2018.
//  Copyright © 2018 Groomefy. All rights reserved.
//

import UIKit

extension String {
    //To check text field or String is blank or not
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    //Validate Email
    func maxLength(length: Int) -> String {
        var str = self
        let nsString = str as NSString
        if nsString.length >= length {
            str = nsString.substring(with:
                NSRange(
                    location: 0,
                    length: nsString.length > length ? length : nsString.length)
            )
        }
        return  str
    }
    
    var isEmail: Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }

    func characterAtIndex(index: Int) -> Character? {
        var cur = 0
        for char in self {
            if cur == index {
                return char
            }
            cur = cur + 1
        }
        return nil
    }
    
    func getEncodedUrl() -> String? {
        let encodedString = self.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: "!*'();:@&=+$|,/?%#[]{} ").inverted) ?? ""
        return encodedString
    }
    
    func getCleanedURL() -> URL? {
        guard self.isEmpty == false else {
            return nil
        }
        if let url = URL(string: self) {
            return url
        } else {
           let urlString =  self.replacingOccurrences(of: "+", with: "%2B")
            if let urlEscapedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) , let escapedURL = URL(string: urlEscapedString){
                return escapedURL
            }
        }
        return nil
    }
    
    func utf8DecodedString()-> String {
        let data = self.data(using: .utf8)
        if let message = String(data: data!, encoding: .nonLossyASCII){
            return message
        }
        return ""
    }
    
    func utf8EncodedString()-> String {
        let messageData = self.data(using: .nonLossyASCII)
        let text = String(data: messageData!, encoding: .utf8) ?? ""
        return text
    }
    
    //validate Password
    var date: Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: self)
        return date!
    }
    
    
    func singleNumberName (number: Int) -> String {
        switch (number) {
        case 1: return "First"
        case 2: return "Second"
        case 3: return "Third"
        case 4: return "Fourth"
        case 5: return "Fifth"
        case 6: return "Sixth"
        case 7: return "Seventh"
        case 8: return "Eighth"
        case 9: return "Nineth"
        case 10: return "Tenth"
        default:return ""
        }
    }
    
        func capitalizingFirstLetter() -> String {
            return prefix(1).uppercased() + self.lowercased().dropFirst()
        }
        
        mutating func capitalizeFirstLetter() {
            self = self.capitalizingFirstLetter()
        }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }

}

extension String {
    func versionToInt() -> String {
        var versionString = ""
        let versionArray = self.components(separatedBy: ".")
        .map { Int.init($0) ?? 0 }
        for (_,value) in versionArray.enumerated() {
            versionString.append("\(value)")
        }
        return versionString
    }
    

    func getWidth(font:UIFont) -> Float{
          let label:UILabel = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width:CGFloat(CGFloat.greatestFiniteMagnitude), height: 30))
          label.numberOfLines = 0
          label.lineBreakMode = NSLineBreakMode.byWordWrapping
          label.font = font
          label.text = self
          label.sizeToFit()
          return Float(label.frame.width)
      }
}


