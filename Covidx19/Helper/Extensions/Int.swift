//
//  Int.swift
//  Mirah
//
//  Created by Vaibhav Singh on 06/09/18.
//  Copyright © 2018 Alok. All rights reserved.
//

import Foundation
extension Int {
   
    func singleNumberName () -> String {
        switch (self) {
        case 1: return "First"
        case 2: return "Second"
        case 3: return "Third"
        case 4: return "Fourth"
        case 5: return "Fifth"
        case 6: return "Sixth"
        case 7: return "Seventh"
        case 8: return "Eighth"
        case 9: return "Nineth"
        case 10: return "Tenth"
        default:return ""
        }
    }
}
