//
//  Font.swift
//  Groomefy WorkSpace
//
//  Created by Vaibhav Singh on 28/02/18.
//  Copyright © 2018 Groomefy. All rights reserved.
//

import UIKit
extension UIFont {
    
    func bold(size : CGFloat) -> UIFont {
        return boldMontserrat(size: size)
    }
    
    func semiBold(size : CGFloat) -> UIFont {
        return semiBoldMontserrat(size: size)
    }
    
    func regular(size : CGFloat) -> UIFont {
        return regularMontserrat(size: size)
    }

    
    func boldGtWalsheim(size : CGFloat) -> UIFont {
        return boldMontserrat(size: size)
    }
    
    func semiBoldGtWalsheim(size : CGFloat) -> UIFont {
        return semiBoldMontserrat(size: size)
    }
    
    func regularGtWalsheim(size : CGFloat) -> UIFont {
        return regularMontserrat(size: size)
    }
    
    func lightGtWalsheim(size : CGFloat) -> UIFont {
        return lightMontserrat(size: size)
    }
    
    
   
    
   
    func boldMontserrat(size : CGFloat) -> UIFont {
        return  UIFont(name: "Montserrat-Bold",
                       size: size)!
    }
    func semiBoldMontserrat(size : CGFloat) -> UIFont {
        return  UIFont(name: "Montserrat-SemiBold",
                       size: size)!
    }
    
    func regularMontserrat(size : CGFloat) -> UIFont {
        return  UIFont(name:"Montserrat-Regular",
                       size: size)!
    }
    func mediumMontserrat(size : CGFloat) -> UIFont {
        return  UIFont(name:"Montserrat-Medium",
                       size: size)!
    }
    func lightMontserrat(size : CGFloat) -> UIFont {
        return  UIFont(name:"Montserrat-Light",
                       size: size)!
    }
    
    func mediumItalicMontserrat(size : CGFloat) -> UIFont {
//        return  UIFont(name:"Montserrat-MediumItalic",
//                       size: size)!
        return  UIFont(name:"Montserrat-Regular",
                       size: size)!

    }
   
    func semiBoldItalicMontserrat(size : CGFloat) -> UIFont {
        return  UIFont(name: "Montserrat-SemiBoldItalic",
                       size: size)!
    }
}
