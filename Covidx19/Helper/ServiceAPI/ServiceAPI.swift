
//
//  ServiceAPI.swift
//  Covidx19
//
//  Created by Vaibhav Singh on 04/04/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit
import Alamofire

class ServiceAPI: NSObject {
    //MARK:- shared instance
    static let shared = ServiceAPI()
    private override init() {}
    
    //MARK:- APIS
    //get stagwise user
    func data(completion: @escaping(_ success: [String:Any]?)->()){
            var headers = HTTPHeaders()
            headers["Content-type"] =  "application/json"

        
            Alamofire.request(API.data,method: .get,
                              parameters: nil,
                              encoding: JSONEncoding.default,
                              headers: headers)
                .responseJSON { response in
                    
                    guard response.result.isSuccess, let _ = response.result.value as? [String:Any]? else {
                        print("Error : \(String(describing: response.result.error))")
                        completion(nil)
                        return
                    }
                    
                    if  let result = response.result.value as? Dictionary<String, Any> {
                        if IS_DEBUG_MODE == true {
//                            print("\(url):\(String(describing: result.jsonString()))")
                        }
                        completion(result)
                        return
                    }
                        else {
                            completion(nil)
                            return

                        }
                    }
        }
    
    
    func state_district_wise_v2(completion: @escaping(_ success: [[String:Any]]?)->()){
            var headers = HTTPHeaders()
            headers["Content-type"] =  "application/json"

        
            Alamofire.request(API.state_district_wise_v2,method: .get,
                              parameters: nil,
                              encoding: JSONEncoding.default,
                              headers: headers)
                .responseJSON { response in
                    
                    guard response.result.isSuccess, let _ = response.result.value as? [[String:Any]]? else {
                        print("Error : \(String(describing: response.result.error))")
                        completion(nil)
                        return
                    }
                    
                    if  let result = response.result.value as? [Dictionary<String, Any>] {
//                        if IS_DEBUG_MODE == true {
                            print(":\(String(describing: result))")
//                        }
                        completion(result)
                        return
                    }
                        else {
                            completion(nil)
                            return

                        }
                    }
        }
}
