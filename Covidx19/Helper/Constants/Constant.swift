//
//  Constant.swift
//  Covidx19
//
//  Created by Vaibhav Singh on 04/04/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import Foundation

enum API {
    static let state_district_wise = "https://api.covid19india.org/state_district_wise.json"
    static let data = "https://api.covid19india.org/data.json"
    static let state_district_wise_v2 = "https://api.covid19india.org/v2/state_district_wise.json"
    static let travel_history = "https://api.covid19india.org/travel_history.json"
    static let raw_data = "https://api.covid19india.org/raw_data.json"
    static let states_daily = "https://api.covid19india.org/states_daily.json"
}

var IS_DEBUG_MODE = true
