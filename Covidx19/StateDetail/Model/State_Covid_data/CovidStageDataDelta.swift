//
//  CovidStageDataDelta.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2020

import Foundation


class CovidStageDataDelta : NSObject {

    var confirmed : Int?


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        confirmed = dictionary["confirmed"] as? Int
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if confirmed != nil{
            dictionary["confirmed"] = confirmed
        }
        return dictionary
    }

    
}