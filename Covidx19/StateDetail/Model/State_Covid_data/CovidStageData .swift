//
//  CovidStageData .swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2020

import Foundation


class CovidStageData  : NSObject {

    var districtData : [CovidStageDataDistrictDatum]?
    var state : String?


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        state = dictionary["state"] as? String
        districtData = [CovidStageDataDistrictDatum]()
        if let districtDataArray = dictionary["districtData"] as? [[String:Any]]{
            for dic in districtDataArray{
                let value = CovidStageDataDistrictDatum(fromDictionary: dic)
                districtData?.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if state != nil{
            dictionary["state"] = state
        }
        if districtData != nil{
            var dictionaryElements = [[String:Any]]()
            for districtDataElement in districtData ?? []{
                dictionaryElements.append(districtDataElement.toDictionary())
            }
            dictionary["districtData"] = dictionaryElements
        }
        return dictionary
    }

    
}