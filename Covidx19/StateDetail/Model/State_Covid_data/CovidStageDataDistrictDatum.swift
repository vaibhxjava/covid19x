//
//  CovidStageDataDistrictDatum.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2020

import Foundation


class CovidStageDataDistrictDatum : NSObject {

    var confirmed : Int?
    var delta : CovidStageDataDelta?
    var district : String?
    var lastupdatedtime : String?


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        confirmed = dictionary["confirmed"] as? Int
        district = dictionary["district"] as? String
        lastupdatedtime = dictionary["lastupdatedtime"] as? String
        if let deltaData = dictionary["delta"] as? [String:Any]{
            delta = CovidStageDataDelta(fromDictionary: deltaData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if confirmed != nil{
            dictionary["confirmed"] = confirmed
        }
        if district != nil{
            dictionary["district"] = district
        }
        if lastupdatedtime != nil{
            dictionary["lastupdatedtime"] = lastupdatedtime
        }
        if delta != nil{
            dictionary["delta"] = delta?.toDictionary()
        }
        return dictionary
    }

    
}
