//
//  DetailViewController.swift
//  Covidx19
//
//  Created by Vaibhav Singh on 04/04/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    //MARK:- IBOutlets
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var deathLabel: UILabel!
    @IBOutlet var recoverLabel: UILabel!
    @IBOutlet var lastUpdateLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    
    //MARK:- Variables
    private var headerData:CovidDataStatewise? {
        didSet {
            self.deathLabel.text = (headerData?.deaths ?? "") + " Deaths"
            self.recoverLabel.text = (headerData?.recovered ?? "") + " Recovered"
            self.lastUpdateLabel.text = "Last updates on " + (headerData?.lastupdatedtime ?? "")
            self.stateLabel.text = headerData?.state ?? ""
        }
    }
    var stateData:CovidDataStatewise?
    var viewModel:HomeViewModel?
    private var covidStateData:CovidStageData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerData = stateData
        
        for item in self.viewModel?.stageViseDataArray ?? [] {
            if item.state == self.stateData?.state {
                self.covidStateData = item
            }
        }
        self.tableView.register(UINib(nibName: "DataTableViewCell", bundle: nil), forCellReuseIdentifier: "DataTableViewCell")
        self.tableView.reloadData()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.covidStateData?.districtData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "DataTableViewCell", for: indexPath) as? DataTableViewCell {
            cell.data =  self.covidStateData?.districtData?[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
