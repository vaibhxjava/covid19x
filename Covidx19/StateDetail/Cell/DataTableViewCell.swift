//
//  DataTableViewCell.swift
//  Covidx19
//
//  Created by Vaibhav Singh on 04/04/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit

class DataTableViewCell: UITableViewCell {
    //MARK:- IBOutlets
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var deathLabel: UILabel!
    @IBOutlet var recoverLabel: UILabel!
    @IBOutlet var lastUpdateLabel: UILabel!
    //MARK:- Variables
    var data:CovidStageDataDistrictDatum? {
        didSet {
            self.stateLabel.text = data?.district
            self.recoverLabel.text = "\(data?.confirmed ?? 0) Total confirmed cases"
            self.lastUpdateLabel.text = "\(data?.lastupdatedtime ?? "")"
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    
}
