//
//  DataCollectionViewCell.swift
//  Covidx19
//
//  Created by Vaibhav Singh on 04/04/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit

class DataCollectionViewCell: UICollectionViewCell {
    //MARK:- IBOutlets
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var deathLabel: UILabel!
    @IBOutlet var recoverLabel: UILabel!
    @IBOutlet var lastUpdateLabel: UILabel!
    @IBOutlet var calenderIcon: UIImageView!
    //MARK:- Variables
    var data:CovidDataStatewise? {
        didSet {
            self.stateLabel.text = data?.state
            self.deathLabel.text = (data?.deaths ?? "") + " Deaths"
            self.recoverLabel.text = (data?.recovered ?? "") + " Recovered"
            self.lastUpdateLabel.text = (data?.confirmed ?? "") + " Total cases"
            self.calenderIcon.isHidden = true
            self.layer.borderWidth = 0
        }
    }
    
    
    var timeData:CovidDataCasesTimeSery? {
        didSet {
            self.stateLabel.text = timeData?.date
            self.deathLabel.text = (timeData?.dailydeceased ?? "") + " Deaths"
            self.recoverLabel.text = (timeData?.dailyrecovered ?? "") + " Recovered"
            self.lastUpdateLabel.text = (timeData?.dailyconfirmed ?? "") + " Total cases"
            self.layer.borderWidth = 1.5
        }
    }
}
