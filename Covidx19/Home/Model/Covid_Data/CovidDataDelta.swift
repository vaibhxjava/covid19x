//
//  CovidDataDelta.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2020

import Foundation


class CovidDataDelta : NSObject {

    var active : Int?
    var confirmed : Int?
    var deaths : Int?
    var recovered : Int?


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        active = dictionary["active"] as? Int
        confirmed = dictionary["confirmed"] as? Int
        deaths = dictionary["deaths"] as? Int
        recovered = dictionary["recovered"] as? Int
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if active != nil{
            dictionary["active"] = active
        }
        if confirmed != nil{
            dictionary["confirmed"] = confirmed
        }
        if deaths != nil{
            dictionary["deaths"] = deaths
        }
        if recovered != nil{
            dictionary["recovered"] = recovered
        }
        return dictionary
    }

}