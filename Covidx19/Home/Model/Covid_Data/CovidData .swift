//
//  CovidData .swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2020

import Foundation


class CovidData  : NSObject {

    var casesTimeSeries : [CovidDataCasesTimeSery]?
    var keyValues : [CovidDataKeyValue]?
    var statewise : [CovidDataStatewise]?
    var tested : [CovidDataTested]?


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        casesTimeSeries = [CovidDataCasesTimeSery]()
        if let casesTimeSeriesArray = dictionary["cases_time_series"] as? [[String:Any]]{
            for dic in casesTimeSeriesArray{
                let value = CovidDataCasesTimeSery(fromDictionary: dic)
                casesTimeSeries?.append(value)
            }
        }
        keyValues = [CovidDataKeyValue]()
        if let keyValuesArray = dictionary["key_values"] as? [[String:Any]]{
            for dic in keyValuesArray{
                let value = CovidDataKeyValue(fromDictionary: dic)
                keyValues?.append(value)
            }
        }
        statewise = [CovidDataStatewise]()
        if let statewiseArray = dictionary["statewise"] as? [[String:Any]]{
            for dic in statewiseArray{
                let value = CovidDataStatewise(fromDictionary: dic)
                statewise?.append(value)
            }
        }
        tested = [CovidDataTested]()
        if let testedArray = dictionary["tested"] as? [[String:Any]]{
            for dic in testedArray{
                let value = CovidDataTested(fromDictionary: dic)
                tested?.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if casesTimeSeries != nil{
            var dictionaryElements = [[String:Any]]()
            for casesTimeSeriesElement in casesTimeSeries ?? [] {
                dictionaryElements.append(casesTimeSeriesElement.toDictionary())
            }
            dictionary["casesTimeSeries"] = dictionaryElements
        }
        if keyValues != nil{
            var dictionaryElements = [[String:Any]]()
            for keyValuesElement in keyValues ?? [] {
                dictionaryElements.append(keyValuesElement.toDictionary())
            }
            dictionary["keyValues"] = dictionaryElements
        }
        if statewise != nil{
            var dictionaryElements = [[String:Any]]()
            for statewiseElement in statewise ?? [] {
                dictionaryElements.append(statewiseElement.toDictionary())
            }
            dictionary["statewise"] = dictionaryElements
        }
        if tested != nil{
            var dictionaryElements = [[String:Any]]()
            for testedElement in tested ?? [] {
                dictionaryElements.append(testedElement.toDictionary())
            }
            dictionary["tested"] = dictionaryElements
        }
        return dictionary
    }

}
