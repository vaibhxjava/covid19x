//
//  CovidDataTested.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2020

import Foundation


class CovidDataTested : NSObject {

    var source : String?
    var testsconductedbyprivatelabs : String?
    var totalindividualstested : String?
    var totalpositivecases : String?
    var totalsamplestested : String?
    var updatetimestamp : String?


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        source = dictionary["source"] as? String
        testsconductedbyprivatelabs = dictionary["testsconductedbyprivatelabs"] as? String
        totalindividualstested = dictionary["totalindividualstested"] as? String
        totalpositivecases = dictionary["totalpositivecases"] as? String
        totalsamplestested = dictionary["totalsamplestested"] as? String
        updatetimestamp = dictionary["updatetimestamp"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if source != nil{
            dictionary["source"] = source
        }
        if testsconductedbyprivatelabs != nil{
            dictionary["testsconductedbyprivatelabs"] = testsconductedbyprivatelabs
        }
        if totalindividualstested != nil{
            dictionary["totalindividualstested"] = totalindividualstested
        }
        if totalpositivecases != nil{
            dictionary["totalpositivecases"] = totalpositivecases
        }
        if totalsamplestested != nil{
            dictionary["totalsamplestested"] = totalsamplestested
        }
        if updatetimestamp != nil{
            dictionary["updatetimestamp"] = updatetimestamp
        }
        return dictionary
    }

}