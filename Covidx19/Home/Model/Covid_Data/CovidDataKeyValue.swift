//
//  CovidDataKeyValue.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2020

import Foundation


class CovidDataKeyValue : NSObject {

    var confirmeddelta : String?
    var counterforautotimeupdate : String?
    var deceaseddelta : String?
    var lastupdatedtime : String?
    var recovereddelta : String?
    var statesdelta : String?


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        confirmeddelta = dictionary["confirmeddelta"] as? String
        counterforautotimeupdate = dictionary["counterforautotimeupdate"] as? String
        deceaseddelta = dictionary["deceaseddelta"] as? String
        lastupdatedtime = dictionary["lastupdatedtime"] as? String
        recovereddelta = dictionary["recovereddelta"] as? String
        statesdelta = dictionary["statesdelta"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if confirmeddelta != nil{
            dictionary["confirmeddelta"] = confirmeddelta
        }
        if counterforautotimeupdate != nil{
            dictionary["counterforautotimeupdate"] = counterforautotimeupdate
        }
        if deceaseddelta != nil{
            dictionary["deceaseddelta"] = deceaseddelta
        }
        if lastupdatedtime != nil{
            dictionary["lastupdatedtime"] = lastupdatedtime
        }
        if recovereddelta != nil{
            dictionary["recovereddelta"] = recovereddelta
        }
        if statesdelta != nil{
            dictionary["statesdelta"] = statesdelta
        }
        return dictionary
    }

}