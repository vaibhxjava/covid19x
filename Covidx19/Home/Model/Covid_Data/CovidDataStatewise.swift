//
//  CovidDataStatewise.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2020

import Foundation


class CovidDataStatewise : NSObject {

    var active : String?
    var confirmed : String?
    var deaths : String?
    var delta : CovidDataDelta?
    var deltaconfirmed : String?
    var deltadeaths : String?
    var deltarecovered : String?
    var lastupdatedtime : String?
    var recovered : String?
    var state : String?
    var statecode : String?


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        active = dictionary["active"] as? String
        confirmed = dictionary["confirmed"] as? String
        deaths = dictionary["deaths"] as? String
        deltaconfirmed = dictionary["deltaconfirmed"] as? String
        deltadeaths = dictionary["deltadeaths"] as? String
        deltarecovered = dictionary["deltarecovered"] as? String
        lastupdatedtime = dictionary["lastupdatedtime"] as? String
        recovered = dictionary["recovered"] as? String
        state = dictionary["state"] as? String
        statecode = dictionary["statecode"] as? String
        if let deltaData = dictionary["delta"] as? [String:Any]{
            delta = CovidDataDelta(fromDictionary: deltaData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if active != nil{
            dictionary["active"] = active
        }
        if confirmed != nil{
            dictionary["confirmed"] = confirmed
        }
        if deaths != nil{
            dictionary["deaths"] = deaths
        }
        if deltaconfirmed != nil{
            dictionary["deltaconfirmed"] = deltaconfirmed
        }
        if deltadeaths != nil{
            dictionary["deltadeaths"] = deltadeaths
        }
        if deltarecovered != nil{
            dictionary["deltarecovered"] = deltarecovered
        }
        if lastupdatedtime != nil{
            dictionary["lastupdatedtime"] = lastupdatedtime
        }
        if recovered != nil{
            dictionary["recovered"] = recovered
        }
        if state != nil{
            dictionary["state"] = state
        }
        if statecode != nil{
            dictionary["statecode"] = statecode
        }
        if delta != nil{
            dictionary["delta"] = delta?.toDictionary()
        }
        return dictionary
    }

}
