//
//  CovidDataCasesTimeSery.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2020

import Foundation


class CovidDataCasesTimeSery : NSObject {

    var dailyconfirmed : String?
    var dailydeceased : String?
    var dailyrecovered : String?
    var date : String?
    var totalconfirmed : String?
    var totaldeceased : String?
    var totalrecovered : String?


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        dailyconfirmed = dictionary["dailyconfirmed"] as? String
        dailydeceased = dictionary["dailydeceased"] as? String
        dailyrecovered = dictionary["dailyrecovered"] as? String
        date = dictionary["date"] as? String
        totalconfirmed = dictionary["totalconfirmed"] as? String
        totaldeceased = dictionary["totaldeceased"] as? String
        totalrecovered = dictionary["totalrecovered"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if dailyconfirmed != nil{
            dictionary["dailyconfirmed"] = dailyconfirmed
        }
        if dailydeceased != nil{
            dictionary["dailydeceased"] = dailydeceased
        }
        if dailyrecovered != nil{
            dictionary["dailyrecovered"] = dailyrecovered
        }
        if date != nil{
            dictionary["date"] = date
        }
        if totalconfirmed != nil{
            dictionary["totalconfirmed"] = totalconfirmed
        }
        if totaldeceased != nil{
            dictionary["totaldeceased"] = totaldeceased
        }
        if totalrecovered != nil{
            dictionary["totalrecovered"] = totalrecovered
        }
        return dictionary
    }

}