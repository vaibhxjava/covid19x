//
//  HomeViewModel.swift
//  Covidx19
//
//  Created by Vaibhav Singh on 04/04/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit
protocol RefreshDataProtocol: NSObject {
    func refreshData()
}

class HomeViewModel: NSObject {
    //MARK:- variables
    var covidData:CovidData?
    var allStateData:CovidDataStatewise?
    var stageViseDataArray:[CovidStageData]? = []
    
    weak var delegate:RefreshDataProtocol? = nil
    
    func getDataStateWise()  {
        ServiceAPI.shared.data() { (response) in
            if response != nil {
                self.covidData = CovidData(fromDictionary: response ?? [:])
                self.delegate?.refreshData()
            }
        }
    }
    
    
    func getDataStateDistrictWise()  {
        stageViseDataArray?.removeAll()
        ServiceAPI.shared.state_district_wise_v2() { (response) in
            if response != nil {
                for dict in response ?? [] {
                    if let item = CovidStageData(fromDictionary: dict) as? CovidStageData {
                        self.stageViseDataArray?.append(item)
                    }
                }
                self.delegate?.refreshData()
            }
        }
    }
}
