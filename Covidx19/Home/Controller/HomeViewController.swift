//
//  HomeViewController.swift
//  Covidx19
//
//  Created by Vaibhav Singh on 04/04/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    //MARK:- IBOutlets
    @IBOutlet var deathLabel: UILabel!
    @IBOutlet var recoverLabel: UILabel!
    @IBOutlet var lastUpdateLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var totalCasesLabel: UILabel!
    
    //MARK:- Variables
    private let refreshControl = UIRefreshControl()
    var headerData:CovidDataStatewise? {
        didSet {
            self.deathLabel.text = (headerData?.deaths ?? "") + " Deaths"
            self.recoverLabel.text = (headerData?.recovered ?? "") + " Recovered"
            self.lastUpdateLabel.text = "Last updates on " + (headerData?.lastupdatedtime ?? "")
            self.totalCasesLabel.text = (headerData?.recovered ?? "") + " Total cases"

        }
    }
    
    private var viewModel = HomeViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshCovidData), for: .valueChanged)
        refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)

        self.collectionView.register(UINib(nibName: "DataCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DataCollectionViewCell")
        self.viewModel.delegate = self
        self.viewModel.getDataStateWise()
        self.viewModel.getDataStateDistrictWise()
        // Do any additional setup after loading the view.
    }
    
    @objc private func refreshCovidData(_ sender: Any) {
        // Fetch Weather Data
        self.viewModel.getDataStateWise()
        self.viewModel.getDataStateDistrictWise()
    }
    
    @IBAction func historyButtonTapped(_ sender: Any) {
        if let vc =  self.storyboard?.instantiateViewController(withIdentifier: "AllDetailViewController") as? AllDetailViewController {
            vc.viewModel = self.viewModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.covidData?.statewise?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DataCollectionViewCell", for: indexPath) as? DataCollectionViewCell {
            cell.data = self.viewModel.covidData?.statewise?[indexPath.item]
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width-10)/2, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vc =  self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController {
            vc.viewModel = self.viewModel
            vc.stateData = self.viewModel.covidData?.statewise?[indexPath.item]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

}

extension HomeViewController: RefreshDataProtocol {
    func refreshData() {
        self.refreshControl.endRefreshing()
        if self.viewModel.covidData?.keyValues?.count ?? 0 > 0 {
            self.viewModel.allStateData = self.viewModel.covidData?.statewise?.first
            self.headerData = self.viewModel.covidData?.statewise?.first
            self.viewModel.covidData?.statewise?.removeFirst()
        }
        self.collectionView.reloadData()
    }
}
