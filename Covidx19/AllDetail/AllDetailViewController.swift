//
//  AllDetailViewController.swift
//  Covidx19
//
//  Created by Vaibhav Singh on 04/04/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit

class AllDetailViewController: UIViewController {
    //MARK:- IBOutlets
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var deathLabel: UILabel!
    @IBOutlet var recoverLabel: UILabel!
    @IBOutlet var lastUpdateLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!

    //MARK:- Variables
    private var headerData:CovidDataStatewise? {
        didSet {
            self.deathLabel.text = (headerData?.deaths ?? "") + " Deaths"
            self.recoverLabel.text = (headerData?.recovered ?? "") + " Recovered"
            self.lastUpdateLabel.text = "Last updates on " + (headerData?.lastupdatedtime ?? "")
            self.stateLabel.text = (headerData?.confirmed ?? "") + " Total cases"

        }
    }
    
    var viewModel:HomeViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerData =  self.viewModel?.allStateData
        // Do any additional setup after loading the view.
        self.collectionView.register(UINib(nibName: "DataCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DataCollectionViewCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()

    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension AllDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel?.covidData?.casesTimeSeries?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DataCollectionViewCell", for: indexPath) as? DataCollectionViewCell {
            cell.timeData = self.viewModel?.covidData?.casesTimeSeries?[indexPath.item]
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width-10)/2, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }

}
